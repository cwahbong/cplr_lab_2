import sys

from compiler import grammar, base, algo

def parse_rules(tokens):
  """ Parse the lexed `grammar' tokens into the dict with this format:
  {
    lhs1: [rhs1_1, rhs1_2, ...],
    lhs2: [rhs2_1, rhs2_2, ...],
    ...
  }
  """
  def _split(l, code, endswithcode=False):
    splitted = []
    sub = []
    for item in l:
      if item.code == code:
        splitted.append(tuple(sub))
        sub = []
      else:
        sub.append(item)
    if not endswithcode:
      splitted.append(tuple(sub))
    return splitted
  result = []
  for irule in _split(tokens, ord(';'), endswithcode=True):
    rule = [irule[0], _split(irule[2:], ord('|'), endswithcode=False)]
    result.append(rule)
  return result

def csv_cell_formatter(cell):
  """ The formatter of the csv cell.  See the spec of this assignment
  for more information.
  """
  def escaped_str(s):
    return s.replace('"', '""')
  def escaped_cell(cell):
    if isinstance(cell, (set, frozenset)):
      def _comp_symbol(x, y):
        def _type_code(s):
          if isinstance(s, algo.SymbolSet.SpecialSymbol):
            return 0
          elif isinstance(s, base.Token):
            return s.code
          else:
            raise ValueError
        cx, cy = _type_code(x), _type_code(y)
        return cmp(cx, cy) if cx != cy else cmp(str(x), str(y))
      elem_str = map(lambda x: x.attribute , sorted(cell, _comp_symbol))
      return escaped_str(' '.join(elem_str))
    elif isinstance(cell, base.Token):
      return escaped_str(cell.attribute)
    elif isinstance(cell, tuple):
      if len(cell) == 0:
        return '(null)'
      else:
        return escaped_str(' '.join(map(lambda x: x.attribute, cell)))
    else:
      raise ValueError
  if cell == None:
    return ''
  else:
    return '" {} "'.format(escaped_cell(cell))

def csv_row(row, cell_formatter=csv_cell_formatter):
  """ Given the row and the cell formatter, return the csv format of
  this row.  Each cell in the row will be formatted by the
  cell formatter.
  """
  return ','.join(map(cell_formatter, row))

def table(lhs, rrules, algo):
  """ Return a table of the rules, containing its first set, follow
  set, and predict set.
  See the spec for more information.
  """
  result = map(lambda rrule: [None, rrule, None, None, algo.predict_set((lhs, rrule))], rrules)
  result[0][0] = lhs
  result[0][2], result[0][3] = algo.first_set(lhs), algo.follow_set(lhs)
  return result

def __main__():
  with open(sys.argv[1], 'r') as input_file:
    ss = algo.SymbolSet(parse_rules(grammar.lexer.tokens(''.join(input_file))))
  with open(sys.argv[2], 'w') as output_file:
    output_file.write('" LHS "," RHS "," FIRST "," FOLLOW "," PREDICT "\n')
    for pair_rule in ss.rules().items():
      for row in table(pair_rule[0], pair_rule[1], ss):
        output_file.write('{}\n'.format(csv_row(row)))

if __name__=='__main__':
  __main__()

