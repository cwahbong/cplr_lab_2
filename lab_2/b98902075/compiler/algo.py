import collections
import functools
import itertools

from compiler import base


def lazy_property(func):
  """ Decorator that makes the property be lazy initialization.
  """
  attr_name = '_lazy_' + func.__name__
  @property
  @functools.wraps(func)
  def _lazy_property(self):
    if not hasattr(self, attr_name):
      setattr(self, attr_name, func(self))
    return getattr(self, attr_name)
  return _lazy_property


class cached(object):
  """ Decorator that makes the result a function be cached.  You can
  safely decorate the inner function.  Do not decorate instance method
  unless you know what are you doing.
  """

  def __init__(self, func):
    self.__func = func
    self.__cache = {}

  def __repr__(self):
    return self.__func.__doc__

  def __call__(self, *args):
    try:
      if args not in self.__cache:
        self.__cache[args] = self.__func(*args)
      return self.__cache[args]
    except TypeError:
      return self.__func(*args)


def depend(S, direct_depend):
  """ Find all element depend by S and direct depend function.  It uses
  breadth first search.
  """
  result = {S}
  to_check = {S}
  while len(to_check)>0:
    for T in {U for U in direct_depend(to_check.pop()) if U not in result}:
      result.add(T)
      to_check.add(T)
  return result


class SymbolSet(object):
  """ SymbolSet can calculate the first set, follow set, predict set.
  Just init the SymbolSet object by the rules.
  """

  class SpecialSymbol(object):

    def __init__(self, attr):
      self.attribute = attr

    def __str__(self):
      return str(self.attribute)
  
  NULL = SpecialSymbol("(null)")
  EOF = SpecialSymbol("(eof)")

  def __init__(self, rules):
    """ Rules is a dict with this format:
    {
      lhs_1: [rhs_1_1, rhs_1_2, ...],
      lhs_2: [rhs_2_1, rhs_2_2, ...],
      ...
    }
    """
    self.__rules = collections.OrderedDict(rules)
    self.__start_symbol = rules[0][0]
    self.__non_terminals = frozenset(map(lambda x: x[0], rules))
    splitted_rules_lists = map(lambda (lhs, rrhs): map(lambda rhs: (lhs, rhs), rrhs), rules)
    self.__splitted_rules = list(itertools.chain.from_iterable(splitted_rules_lists))

  @lazy_property
  def __derive_lambda_symbols(self):
    """ Return the set of all symbols that can derive lambda.
    """
    def _check_lambda(counter, rule, to_check):
      if counter[rule] == 0:
        result.add(rule[0])
        to_check.add(rule[0])
        del counter[rule]
    result = set()
    to_check = set()
    counter = dict(map(lambda x: (x, 0), self.__splitted_rules)) 
    # first derive
    for rule in counter.keys():
      counter[rule] = len(rule[1])
      _check_lambda(counter, rule, to_check)
    # iterative derive
    while len(to_check)>0:
      s = to_check.pop()
      for rule in counter.keys():
        if s == rule[0]:
          del counter[rule]
        elif s in rule[1] and rule in counter:
          counter[rule] -= rule[1].count(s)
          _check_lambda(counter, rule, to_check)
    return frozenset(result)

  def __first_set_helper(self, S, do_isterminal=None, do_notterminal=None):
    result = set()
    for rhs in self.__rules[S]:
      for r in rhs:
        if self.isterminal(r):
          if do_isterminal:
            do_isterminal(result, r)
          break
        else:
          if do_notterminal:
            do_notterminal(result, r)
          if not self.derive_lambda(r):
            break
    return result
  
  def rules(self):
    """ Return the rule data.
    """
    return collections.OrderedDict(self.__rules)

  def non_terminals(self):
    """ Return all non terminal symbols.
    """
    return self.__non_terminals

  def isterminal(self, S):
    """ Return True if the symbol S is terminal.
    """
    return S not in self.__non_terminals

  def derive_lambda(self, S):
    """ Given symbol S, return True if it will derive empty string.
    """
    return S in self.__derive_lambda_symbols
 
  def first_set(self, S):
    """ Given symbol, or a tuple of symbols S, return its first set.
    """
    @cached
    def _first_set_direct_depend(S):
      return frozenset(self.__first_set_helper(S, do_notterminal=set.add))
    
    @cached
    def _first_set_depend(S):
      return frozenset(depend(S, _first_set_direct_depend))
    
    @cached
    def _self_first_set(S):
      return frozenset(self.__first_set_helper(S, do_isterminal=set.add))

    @cached
    def _single_first_set(S):
      """ The first set of a symbol.
      """
      if self.isterminal(S):
        return {S}
      else:
        result = frozenset({SymbolSet.NULL}) if self.derive_lambda(S) else frozenset()
        return result.union(*map(_self_first_set, _first_set_depend(S)))

    if isinstance(S, base.Token):
      return _single_first_set(S)
    elif isinstance(S, tuple):
      if len(S) == 0:
        return {SymbolSet.NULL}
      result = set()
      for R in S:
        result |= _single_first_set(R)
        if SymbolSet.NULL not in result:
          break
        result.remove(SymbolSet.NULL)
      else:
        result.add(SymbolSet.NULL)
      return result
    else:
      raise ValueError
 
  def follow_set(self, S):
    """ Given a nonterminal symbol S, return its follow set.
    It will raise ValueError if S is a terminal.
    """

    def _tail(rhs, S):
      """ The longest right part of rhs that does not including S
      """
      rrhs = rhs[::-1]
      return rrhs[:rrhs.index(S)][::-1]

    def _tail_first_set(rhs, S):
      return self.first_set(_tail(rhs, S))

    @cached
    def _self_follow_set(S):
      self_follow_rule = filter(lambda (lhs, rhs): S in rhs, self.__splitted_rules)
      self_follows = map(lambda (lhs, rhs): _tail_first_set(rhs, S) - {SymbolSet.NULL}, self_follow_rule)
      result = frozenset({SymbolSet.EOF}) if S == self.__start_symbol else frozenset()
      return result.union(*self_follows)

    @cached
    def _follow_set_direct_depend(S):
      def depend(rule):
        lhs, rhs = rule
        return S in rhs and SymbolSet.NULL in _tail_first_set(rhs, S)
      return frozenset(map(lambda (lhs, rhs): lhs, filter(depend, self.__splitted_rules)))

    @cached
    def _follow_set_depend(S):
      return frozenset(depend(S, _follow_set_direct_depend))

    if self.isterminal(S):
      raise ValueError
    return frozenset().union(*map(_self_follow_set, _follow_set_depend(S)))

  def predict_set(self, rule):
    """ Given a simple rule inside the rule object, return its predict
    set.  The format of the simple rule is (lhs, rhs).
    """
    lhs, rhs = rule
    result = self.first_set(rhs)
    if SymbolSet.NULL in result:
      result.remove(SymbolSet.NULL)
      result |= self.follow_set(lhs)
    return frozenset(result)


