import itertools
import string
import re

from compiler.base import Lexer

def __id__attribute__(token):
  """ The attribute of the id is itself.
  """
  return token

def __num__attribute__(token):
  """ The attribute of the number is its numeral value.
  """
  return int(token)

def __string__attribute__(token):
  """ The attribute of the string is the string without the surrounding
  double quotes, and remove the slash before '\' and '"' character.
  """
  def remove_slash(s):
    return s[1:] if s==r'\\' or s==r'\"' else s
  if token[0]!='"' or token[-1]!='"':
    raise ValueError('String token must surrounded with double quotes.')
  return ''.join(map(remove_slash , re.findall(r'\\\\|\\\"|.', token[1:-1])))

""" Longer (length=2) operator and puctuations should match first.  Some
of the operators and punctuations are special characters for regular
expression.  So it should be escaped.
"""
__op_punc2__ = [
  ('&&', 256),
  ('||', 257),
  ('<=', 258),
  ('>=', 259),
  ('==', 260),
  ('!=', 261)
]
__op_punc2__ = map(lambda x: (re.compile(re.escape(x[0])), x[1]), __op_punc2__)

__op_punc__ = r'+-*/!<>()[]{},;='
__op_punc__ = map(lambda x: (re.compile(re.escape(x)), ord(x)), __op_punc__)

""" Keyword is a special kind of identifier.
"""
__keyword__ = [
  ('int',      262),
  ('if',       263),
  ('else',     264),
  ('while',    265),
  ('break',    266),
  ('continue', 267),
  ('scan',     268),
  ('print',    269),
  ('println',  270)
]
__keyword__ = map(lambda x: (re.compile('^'+x[0]+'$'), x[1]), __keyword__)

""" Identifier starts with an alphabet, trailing with some '\w' characters.
"""
__identifier__ = [
  (re.compile(r'[a-zA-Z]\w*'), 271, __id__attribute__)  # id
]

""" There are only two kind of literals: number and string.
"""
__literal__ = [
  (re.compile(r'\d+'), 272, __num__attribute__), # num
  (re.compile(r'"([{}]|\\\\|\\\")*?"'.format(re.escape(str(string.printable).replace('\\','')))), 273, __string__attribute__) # string
]

""" Combine the token rules into the cmm rule.
"""
__cmm_rule__ = list(itertools.chain(
  __op_punc2__,
  __op_punc__,
  [(re.compile(r'[a-zA-Z]\w*'), list(itertools.chain(
    __keyword__,
    __identifier__
  )))],
  __literal__
))

""" The skip pattern include comments and whitespace characters.
"""
__cmm_skip_pattern__ = re.compile(r'\/\*.*?\*\/|\s+', re.DOTALL)

""" Construct the cmm lexer from its rule and skip pattern.
"""
lexer = Lexer(__cmm_rule__, __cmm_skip_pattern__)


