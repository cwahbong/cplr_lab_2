import itertools
import re
import string

from compiler.base import Lexer

def __id__attribute__(token):
  """ The attribute of the id is itself.
  """
  return token

def __lexeme__attribute__(token):
  """ The attribute of the string is the string without the surrounding
  double quotes, and remove the slash before '\' and '"' character.
  """
  def remove_slash(s):
    return s[1:] if s==r'\\' or s==r'\"' else s
  if token[0]!='"' or token[-1]!='"':
    raise ValueError('String token must surrounded with double quotes.')
  return ''.join(map(remove_slash , re.findall(r'\\\\|\\\"|.', token[1:-1])))


__meta_char__ = r':|;'
__meta_char__ = map(lambda x: (re.compile(re.escape(x)), ord(x)), __meta_char__)

__identifier__ = [
  (re.compile(r'[a-zA-Z]\w*'), 256, __id__attribute__), # id
]

__lexeme__ = [
  (re.compile(r'"([{}]|\\\\|\\\")*?"'.format(re.escape(str(string.printable).replace('\\','')))), 257, __lexeme__attribute__) # lexeme, represented as string
]

__rule__ = list(itertools.chain(
  __meta_char__,
  __identifier__,
  __lexeme__
))

__skip_pattern__ = re.compile(r'\/\*.*?\*\/|\s+', re.DOTALL)

lexer = Lexer(__rule__, __skip_pattern__)

